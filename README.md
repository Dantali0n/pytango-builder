# pytango-builder

Docker images for building [PyTango](https://gitlab.com/tango-controls/pytango). These images do not include PyTango, but rather its dependencies.

One of the uses is as a base for building binary wheels.

## Testing Locally

Before pushing, invoke locally with relevant arguments.

```sh
docker buildx create --platform x86_64 --use
docker buildx build \
	--platform x86_64 \
	--build-arg IMAGE_SOURCE="quay.io/pypa/manylinux2014_x86_64" \
	--build-arg PYTHON_VERSIONS="3.9,3.10,3.11,3.12" \
	--build-arg TANGO_IDL_TAG="6.0" \
	--build-arg TANGO_TAG="10.0.0-rc1" \
	--build-arg BOOST_VER="1.85.0" \
	--build-arg OMNIORB_VER="4.3.2" \
	--build-arg OMNIORB_CONFIGURE_OPTIONS="--build x86_64-unknown-linux-gnu" \
	--build-arg LIBZMQ_VER="v4.3.5" \
	--build-arg CPPZMQ_VER="v4.10.0" \
	--build-arg TANGOTEST_VER="3.9" \
	--build-arg JPEG_TURBO_VER="3.0.0" \
	--build-arg OPENSSL_VERSION="3.3.0" \
	--build-arg OPENSSL_SHA256="...replace..." \
	--build-arg CXX_STANDARD="17" \
	--build-arg ABSEIL_VERSION="20240116.2" \
	--build-arg PROTOBUF_VERSION="v27.0" \
	--build-arg C_ARES_VERSION="cares-1_29_0" \
	--build-arg RE2_VERSION="2024-05-01" \
	--build-arg CURL_VERSION="curl-8_8_0" \
	--build-arg LIBCURL_SYSTEM_DIR="/lib64" \
	--build-arg GRPC_VERSION="v1.64.0" \
	--build-arg OPENTELEMETRY_VERSION="v1.15.0" \
	--tag pytango_builder_test \
	--load \
	.
```
