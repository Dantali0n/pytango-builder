ARG IMAGE_SOURCE

# use tagged version here for consistency, and to keep cache valid
FROM ${IMAGE_SOURCE}:2024-06-09-9bdc70d AS cpp_dependencies

# prepare env
ENV PKG_CONFIG_PATH="/usr/local/lib/pkgconfig:/usr/local/lib64/pkgconfig"
RUN echo "/usr/local/lib64" >> /etc/ld.so.conf.d/01-pytango.conf \
    && mkdir /src \
    && yum install -y python-devel perl-IPC-Cmd \
    && date '+%Y-%m-%d %H:%M:%S' > /build_start_time.txt

# Common CXX standard
ARG CXX_STANDARD

# Install abseil
ARG ABSEIL_VERSION
RUN mkdir /src/abseil/ \
    && cd /src/abseil/ \
    && curl -L -o abseil.tar.gz https://github.com/abseil/abseil-cpp/archive/refs/tags/${ABSEIL_VERSION}.tar.gz \
    && tar xzf abseil.tar.gz --strip-components=1 \
    && rm -rf abseil.tar.gz \
    && mkdir /src/abseil/build \
    && cd /src/abseil/build \
    && cmake \
      -DBUILD_TESTING=OFF \
      -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX=/usr/local \
      -DCMAKE_POSITION_INDEPENDENT_CODE=ON \
      -DBUILD_SHARED_LIBS=ON \
      -DABSL_ENABLE_INSTALL=ON \
      -DABSL_PROPAGATE_CXX_STD=ON \
      -DCMAKE_CXX_STANDARD=${CXX_STANDARD} \
      ..\
    && cmake --build . --target install \
    && ldconfig \
    && cd /src \
    && rm -rf /src/abseil

# Install protobuf
ARG PROTOBUF_VERSION
RUN mkdir /src/protobuf/ \
    && cd /src/protobuf/ \
    && curl -L -o protobuf.tar.gz https://github.com/protocolbuffers/protobuf/archive/refs/tags/${PROTOBUF_VERSION}.tar.gz \
    && tar xzf protobuf.tar.gz --strip-components=1 \
    && rm -rf protobuf.tar.gz \
    && mkdir /src/protobuf/build \
    && cd /src/protobuf/build \
    && cmake \
      -Dprotobuf_BUILD_TESTS=OFF \
      -DCMAKE_BUILD_TYPE=RelWithDebInfo \
      -DCMAKE_INSTALL_PREFIX=/usr/local \
      -DCMAKE_POSITION_INDEPENDENT_CODE=ON \
      -DBUILD_SHARED_LIBS=ON \
      -Dprotobuf_ABSL_PROVIDER=package \
      -Dprotobuf_BUILD_EXAMPLES=OFF \
      -DCMAKE_CXX_STANDARD=${CXX_STANDARD} \
      .. \
    && cmake --build . --target install \
    && ldconfig \
    && cd /src \
    && rm -rf /src/protobuf

# Install c-ares
ARG C_ARES_VERSION
RUN mkdir /src/c-ares/ \
    && cd /src/c-ares/ \
    && curl -L -o c-ares.tar.gz https://github.com/c-ares/c-ares/archive/refs/tags/${C_ARES_VERSION}.tar.gz \
    && tar xzf c-ares.tar.gz --strip-components=1 \
    && rm -rf c-ares.tar.gz \
    && mkdir /src/c-ares/build \
    && cd /src/c-ares/build \
    && cmake \
      -DCARES_BUILD_TESTS=OFF \
      -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX=/usr/local \
      -DCARES_SHARED=ON \
      -DCMAKE_CXX_STANDARD=${CXX_STANDARD} \
      .. \
    && cmake --build . --target install \
    && ldconfig \
    && cd /src \
    && rm -rf /src/c-ares

# Install re2
ARG RE2_VERSION
RUN mkdir /src/re2/ \
    && cd /src/re2/ \
    && curl -L -o re2.tar.gz https://github.com/google/re2/archive/refs/tags/${RE2_VERSION}.tar.gz \
    && tar xzf re2.tar.gz --strip-components=1 \
    && rm -rf re2.tar.gz \
    && mkdir /src/re2/build \
    && cd /src/re2/build \
    && cmake \
      -DRE2_BUILD_TESTING=OFF \
      -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX=/usr/local \
      -DBUILD_SHARED_LIBS=ON \
      -DCMAKE_CXX_STANDARD=${CXX_STANDARD} \
      .. \
    && cmake --build . --target install \
    && ldconfig \
    && cd /src \
    && rm -rf /src/re2

# Install OpenSSL
ARG OPENSSL_VERSION
ARG OPENSSL_SHA256
ARG OPENSSL_CONFIGURE_OPTIONS=""
RUN mkdir /src/openssl/ \
    && cd /src/openssl/ \
    && curl -L -o openssl.tar.gz https://www.openssl.org/source/openssl-${OPENSSL_VERSION}.tar.gz \
    && sha256sum openssl.tar.gz | grep ${OPENSSL_SHA256} \
    && tar xzf openssl.tar.gz --strip-components=1 \
    && rm -rf openssl.tar.gz \
    # make the shlib version unique so the dynamic linker doesn't load the system version
    && sed -i "s/^SHLIB_VERSION=.*/SHLIB_VERSION=177/" VERSION.dat \
    && cd /src/openssl \
    && ./Configure \
    ${OPENSSL_CONFIGURE_OPTIONS} \
    shared \
    no-ssl3 \
    no-ssl3-method \
    no-zlib \
    no-module \
    no-comp \
    no-dynamic-engine \
    no-apps \
    no-docs \
    no-sm2-precomp \
    -fPIC \
    --prefix=/usr/local \
    --openssldir=/opt/pytango/ssl \
    && make -j"$(nproc)" \
    && make install_sw install_ssldirs \
    && ldconfig \
    && cd /src \
    && rm -rf /src/openssl

# Install gRPC
ARG GRPC_VERSION
RUN mkdir /src/grpc/ \
    && cd /src/grpc/ \
    && curl -L -o grpc.tar.gz https://github.com/grpc/grpc/archive/refs/tags/${GRPC_VERSION}.tar.gz \
    && tar xzf grpc.tar.gz --strip-components=1 \
    && rm -rf grpc.tar.gz \
    && mkdir /src/grpc/build \
    && cd /src/grpc/build \
    && cmake \
      -DgRPC_BUILD_TESTS=OFF \
      -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX=/usr/local \
      -DCMAKE_POSITION_INDEPENDENT_CODE=ON \
      -DBUILD_SHARED_LIBS=ON \
      -DgRPC_ABSL_PROVIDER=package \
      -DgRPC_CARES_PROVIDER=package \
      -DgRPC_PROTOBUF_PROVIDER=package \
      -DgRPC_RE2_PROVIDER=package \
      -DgRPC_SSL_PROVIDER=package \
      -DgRPC_ZLIB_PROVIDER=package \
      -DgRPC_INSTALL=ON \
      -DCMAKE_CXX_STANDARD=${CXX_STANDARD} \
      .. \
    && cmake --build . --target install \
    && ldconfig \
    && cd /src \
    && rm -rf /src/grpc

# Install curl
# Note: after installing this version of curl, the system version is broken unless
#       we set LD_LIBRARY_PATH=/lib64 or LD_LIBRARY_PATH=/lib (depending on the platform)
#       so we do that later in the Dockerfile when using curl to download files,
#       using the env var LIBCURL_SYSTEM_DIR.
ARG CURL_VERSION
ARG LIBCURL_SYSTEM_DIR=/lib64
RUN mkdir /src/curl/ \
    && cd /src/curl/ \
    && curl -L -o curl.tar.gz https://github.com/curl/curl/archive/refs/tags/${CURL_VERSION}.tar.gz \
    && tar xzf curl.tar.gz --strip-components=1 \
    && rm -rf curl.tar.gz \
    && mkdir /src/curl/build \
    && cd /src/curl/build \
    && cmake \
      -DBUILD_TESTING=OFF \
      -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX=/usr/local \
      -DCMAKE_POSITION_INDEPENDENT_CODE=ON \
      -DBUILD_SHARED_LIBS=ON \
      -DCURL_ENABLE_SSL=ON \
      -DCURL_USE_OPENSSL=ON \
      -DCURL_ZLIB=ON \
      -DBUILD_CURL_EXE=OFF \
      -DENABLE_CURL_MANUAL=OFF \
      -DBUILD_MISC_DOCS=OFF \
      -DBUILD_LIBCURL_DOCS=OFF \
      .. \
    && cmake --build . --target install \
    && ldconfig \
    && cd /src \
    && rm -rf /src/curl

# Install opentelemetry
ARG OPENTELEMETRY_VERSION
RUN mkdir /src/opentelemetry/ \
    && cd /src/opentelemetry/ \
    && LD_LIBRARY_PATH=${LIBCURL_SYSTEM_DIR} curl -L -o opentelemetry.tar.gz https://github.com/open-telemetry/opentelemetry-cpp/archive/refs/tags/${OPENTELEMETRY_VERSION}.tar.gz \
    && tar xzf opentelemetry.tar.gz --strip-components=1 \
    && rm -rf opentelemetry.tar.gz \
    && mkdir /src/opentelemetry/build \
    && cd /src/opentelemetry/build \
    && cmake \
      -DBUILD_TESTING=OFF \
      -DCMAKE_BUILD_TYPE=RelWithDebInfo \
      -DCMAKE_INSTALL_PREFIX=/usr/local \
      -DCMAKE_POSITION_INDEPENDENT_CODE=ON \
      -DBUILD_SHARED_LIBS=ON \
      -DWITH_OTLP_GRPC=ON \
      -DWITH_OTLP_HTTP=ON \
      -DWITH_ABSEIL=ON \
      -DWITH_BENCHMARK=OFF \
      -DWITH_EXAMPLES=OFF \
      -DWITH_FUNC_TESTS=OFF \
      -DCMAKE_CXX_STANDARD=${CXX_STANDARD} \
      .. \
    && cmake --build . --target install \
    && ldconfig \
    && cd /src \
    && rm -rf /src/opentelemetry

# Install libzmq
ARG LIBZMQ_VER
RUN git clone -b ${LIBZMQ_VER} https://github.com/zeromq/libzmq /src/libzmq \
    && mkdir /src/libzmq/build \
    && cd /src/libzmq/build \
    && cmake \
        -DENABLE_DRAFTS=OFF \
        -DWITH_DOC=OFF \
        -DZMQ_BUILD_TESTS=OFF \
        .. \
    && make -j"$(nproc)" install \
    && ldconfig \
    && cd /src \
    && rm -rf /src/libzmq

# Install cppzmq
ARG CPPZMQ_VER
RUN git clone -b ${CPPZMQ_VER} https://github.com/zeromq/cppzmq /src/cppzmq \
    && mkdir /src/cppzmq/build \
    && cd /src/cppzmq/build \
    && cmake -DCPPZMQ_BUILD_TESTS=OFF .. \
    && make -j"$(nproc)" install \
    && cd /src \
    && rm -rf /src/cppzmq

# Install omniorb
ARG OMNIORB_VER
ARG OMNIORB_CONFIGURE_OPTIONS
RUN mkdir /src/omniorb \
    && cd /src/omniorb \
    && LD_LIBRARY_PATH=${LIBCURL_SYSTEM_DIR} curl -L -o omniorb.tar.bz2 https://sourceforge.net/projects/omniorb/files/omniORB/omniORB-${OMNIORB_VER}/omniORB-${OMNIORB_VER}.tar.bz2/download \
    && tar xjf omniorb.tar.bz2 --strip-components=1 \
    && rm -rf omniorb.tar.bz2 \
    && ./configure \
        $OMNIORB_CONFIGURE_OPTIONS \
        --with-openssl=/usr/local \
    && make -j"$(nproc)" \
    && make install \
    && ldconfig \
    && cd /src \
    && rm -rf /src/omniorb

# Install jpeg-turbo
ARG JPEG_TURBO_VER
RUN git clone https://github.com/libjpeg-turbo/libjpeg-turbo.git --branch ${JPEG_TURBO_VER}  /src/libjpeg-turbo \
    && mkdir /src/libjpeg-turbo/build \
    && cd /src/libjpeg-turbo/build \
    && cmake \
        -DWITH_TURBOJPEG=ON \
        -DCMAKE_INSTALL_PREFIX=/usr/local \
        .. \
    && make -j"$(nproc)" install \
    && ldconfig \
    && cd /src \
    && rm -rf /src/libjpeg-turbo

# Install tango-idl
ARG TANGO_IDL_TAG
RUN git clone https://gitlab.com/tango-controls/tango-idl --branch ${TANGO_IDL_TAG} /src/tango-idl \
    && mkdir /src/tango-idl/build \
    && cd /src/tango-idl/build \
    && cmake .. \
    && make -j"$(nproc)" install \
    && cd /src \
    && rm -rf /src/tango-idl

# Install cppTango
ARG TANGO_TAG
RUN git clone --recurse-submodules https://gitlab.com/tango-controls/cppTango --branch ${TANGO_TAG} /src/cppTango \
    && mkdir /src/cppTango/build \
    && cd /src/cppTango/build \
    && cmake \
        -DBUILD_TESTING=OFF \
        -DCMAKE_BUILD_TYPE=RelWithDebInfo \
        -DCMAKE_INSTALL_PREFIX=/usr/local \
        -DTANGO_USE_TELEMETRY=ON \
        -DCMAKE_CXX_STANDARD=${CXX_STANDARD} \
        .. \
    && make -j"$(nproc)" install \
    && ldconfig \
    && cd /src \
    && rm -rf /src/cppTango

# Install tango-test
ARG TANGOTEST_VER
RUN git clone --recurse-submodules -b ${TANGOTEST_VER} --depth 1 https://gitlab.com/tango-controls/TangoTest.git /src/TangoTest \
    && mkdir /src/TangoTest/build \
    && cd /src/TangoTest/build \
    && cmake \
        -DCMAKE_BUILD_TYPE=RelWithDebInfo \
        -DCMAKE_INSTALL_PREFIX=/usr/local \
        -DCMAKE_CXX_STANDARD=${CXX_STANDARD} \
        .. \
    && make -j"$(nproc)" install \
    && ldconfig \
    && cd /src \
    && rm -rf /src/TangoTest

# create tarball with only the new files and symbolic links in /usr/local, and cleanup
RUN cd /usr/local \
    && find . -type f -o -type l -newermt "$(cat /build_start_time.txt)" | tar -czf /new_usr_local.tgz -T - \
    && rm -rf /src

# =========================================================================================
# For final build of Python dependencies (and PyTango later), use latest image
FROM ${IMAGE_SOURCE}:latest

ENV PKG_CONFIG_PATH="/usr/local/lib/pkgconfig:/usr/local/lib64/pkgconfig"
ARG LIBCURL_SYSTEM_DIR=/lib64
COPY --from=cpp_dependencies /new_usr_local.tgz /new_usr_local.tgz
RUN cd /usr/local \
    && tar zxvf /new_usr_local.tgz \
    && rm /new_usr_local.tgz \
    && echo "/usr/local/lib64" >> /etc/ld.so.conf.d/01-pytango.conf \
    && ldconfig

# Install boost.python (for all python versions)
ARG PYTHON_VERSIONS
ARG BOOST_VER
COPY ./boost /src/boost-extras
RUN mkdir /src/boost/ \
    && cd /src/boost/ \
    && LD_LIBRARY_PATH=${LIBCURL_SYSTEM_DIR} curl -L -o boost.tar.bz2 https://boostorg.jfrog.io/artifactory/main/release/${BOOST_VER}/source/boost_$(echo $BOOST_VER | tr '.' '_').tar.bz2 \
    && tar xjf boost.tar.bz2 --strip-components=1 \
    && rm -rf boost.tar.bz2 \
    # patch for NumPy 2.0 support - https://github.com/conda-forge/boost-feedstock/pull/207
    && patch -uN /src/boost/libs/python/src/numpy/dtype.cpp /src/boost-extras/support-numpy-2.0.0b1.patch \
    && cp /src/boost-extras/user-config.jam /src/boost/tools/build/src/user-config.jam \
    && cd /src/boost \
    && ./bootstrap.sh \
        --prefix=/usr/local \
        --with-libraries=python \
        --with-python=/opt/python/cp39-cp39/bin/python \
    && ./b2 install python=${PYTHON_VERSIONS} \
    && cd /src \
    && rm -rf /src/boost

WORKDIR /

# cleanup
RUN rm -rf /src
